package com.dryl.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import com.dryl.domain.Card;
import com.dryl.domain.Password;
import com.dryl.hateoas.CardDto;
import com.dryl.hateoas.PasswordDto;
import com.dryl.hateoas.UserDto;
import com.dryl.service.CardService;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CardController {

  CardService cardService;

  public CardController(CardService cardService) {
    this.cardService = cardService;
  }

  @GetMapping(value = "/api/cards")
  public ResponseEntity<List<CardDto>> getAllCards() {
    List<Card> cardList = cardService.getAllCards();
    Link link = linkTo(methodOn(CardController.class).getAllCards()).withSelfRel();
    List<CardDto> cardDtos = new ArrayList();
    for (Card entity : cardList) {
      Link selfLink = new Link(link.getHref() + "/" + entity.getNumber()).withSelfRel();
      CardDto cardDto = new CardDto(entity, selfLink);
      cardDtos.add(cardDto);
    }
    return new ResponseEntity<>(cardDtos, HttpStatus.OK);
  }

  @GetMapping(value = "/api/cards/{cardNumber}")
  public ResponseEntity<CardDto> getCard(@PathVariable Long cardNumber) {
    Card card = cardService.getCard(cardNumber);
    Link link = linkTo(methodOn(CardController.class).getCard(cardNumber)).withSelfRel();
    CardDto cardDto = new CardDto(card, link);
    return new ResponseEntity<>(cardDto, HttpStatus.OK);
  }

  @GetMapping(value = "/api/cards/user/{userId}")
  public ResponseEntity<Set<CardDto>> getCardsByUserID(@PathVariable Long userId) {
    Set<Card> cardList = cardService.getCardByUserID(userId);
    Link link = linkTo(methodOn(CardController.class).getAllCards()).withSelfRel();
    Set<CardDto> cardDtos = new HashSet<>();
    for (Card entity : cardList) {
      Link selfLink = new Link(link.getHref() + "/" + entity.getNumber()).withSelfRel();
      CardDto cardDto = new CardDto(entity, selfLink);
      cardDtos.add(cardDto);
    }
    return new ResponseEntity<>(cardDtos, HttpStatus.OK);
  }
}
