package com.dryl.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import com.dryl.domain.Password;
import com.dryl.domain.User;
import com.dryl.hateoas.PasswordDto;
import com.dryl.hateoas.UserDto;
import com.dryl.service.PasswordService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PasswordController {
  PasswordService passwordService;

  public PasswordController(PasswordService passwordService) {
    this.passwordService = passwordService;
  }
  @GetMapping(value = "/api/passwords")
  public ResponseEntity<List<PasswordDto>> getAllPassword(){
    List<Password> passwordList = passwordService.getAllPasswords();
    Link link = linkTo(methodOn(PasswordController.class).getAllPassword()).withSelfRel();
    List<PasswordDto> passwordDtos= new ArrayList();
    for (Password entity:passwordList) {
      Link selfLink = new Link(link.getHref()+"/"+entity.getId()).withSelfRel();
      PasswordDto passwordDto = new PasswordDto(entity,selfLink);
      passwordDtos.add(passwordDto);
    }
    return new ResponseEntity<>(passwordDtos,HttpStatus.OK);
  }
  @GetMapping(value = "/api/passwords/{passwordId}")
  public ResponseEntity<PasswordDto>getPassword(@PathVariable Long passwordId){
    Password password = passwordService.getPassword(passwordId);
    Link link = linkTo(methodOn(PasswordController.class).getPassword(passwordId)).withSelfRel();
    PasswordDto passwordDto = new PasswordDto(password,link);
    return new ResponseEntity<>(passwordDto,HttpStatus.OK);
  }

  @PostMapping(value = "/api/passwords")
  public ResponseEntity<PasswordDto> addPassword(@RequestBody Password newPassword){
    passwordService.createPassword(newPassword);
    Link link = linkTo(methodOn(PasswordController.class).getPassword(newPassword.getId())).withSelfRel();
    PasswordDto passwordDto = new PasswordDto(newPassword,link);
    return new ResponseEntity<>(passwordDto,HttpStatus.CREATED);
  }
  @PutMapping(value = "/api/passwords/{passwordId}")
  public ResponseEntity<PasswordDto> updatePassword(@RequestBody Password uPassword,@PathVariable Long passwordId){
    passwordService.updatePassword(uPassword, passwordId);
    Password password = passwordService.getPassword(passwordId);
    Link link = linkTo(methodOn(PasswordController.class).getPassword(passwordId)).withSelfRel();
    PasswordDto passwordDto = new PasswordDto(password,link);
    return new ResponseEntity<>(passwordDto,HttpStatus.OK);
  }
  @DeleteMapping(value = "api/passwords/{passwordId}")
  public ResponseEntity<PasswordDto> deletePassword(@PathVariable Long passwordId){
    passwordService.deletePassword(passwordId);
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
