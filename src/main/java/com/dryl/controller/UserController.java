package com.dryl.controller;

import com.dryl.domain.User;
import com.dryl.hateoas.UserDto;
import com.dryl.service.UserService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class UserController {
  UserService userService;

  public UserController(UserService userService) {
    this.userService = userService;
  }
  @GetMapping(value = "/api/users")
  public ResponseEntity<List<UserDto>> getAllUser(){
    List<User> userList = userService.getAllUsers();
    Link link = linkTo(methodOn(UserController.class).getAllUser()).withSelfRel();
    List<UserDto> userDtos= new ArrayList();
    for (User entity:userList) {
      Link selfLink = new Link(link.getHref()+"/"+entity.getId()).withSelfRel();
      UserDto userDto = new UserDto(entity,selfLink);
      userDtos.add(userDto);
    }
    return new ResponseEntity<>(userDtos, HttpStatus.OK);
  }
  @GetMapping(value = "/api/users/{userId}")
  public ResponseEntity<UserDto> getUser(@PathVariable Long userId){
    User user = userService.getUser(userId);
    Link link = linkTo(methodOn(UserController.class).getUser(userId)).withSelfRel();
    UserDto userDto = new UserDto(user,link);
    return new ResponseEntity<>(userDto,HttpStatus.OK);
  }

  @PostMapping(value = "/api/users")
  public ResponseEntity<UserDto>addUser(@RequestBody User newUser){
    userService.createUser(newUser);
    Link link = linkTo(methodOn(UserController.class).getUser(newUser.getId())).withSelfRel();
    UserDto userDto = new UserDto(newUser,link);
    return new ResponseEntity<>(userDto,HttpStatus.CREATED);
  }
  @PutMapping(value = "/api/users/{userId}")
  public ResponseEntity<UserDto> updateUser(@RequestBody User uUser, @PathVariable Long userId){
    userService.updateUser(uUser,userId);
    User user = userService.getUser(userId);
    Link link = linkTo(methodOn(UserController.class).getUser(userId)).withSelfRel();
    UserDto userDto = new UserDto(user,link);
    return new ResponseEntity<>(userDto,HttpStatus.OK);
  }
  @DeleteMapping(value = "/api/users/{userId}")
  public ResponseEntity<UserDto>deleteUser(@PathVariable Long userId){
    userService.deleteUser(userId);
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
