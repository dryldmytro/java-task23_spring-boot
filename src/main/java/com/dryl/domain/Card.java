package com.dryl.domain;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NaturalId;

@Entity
public class Card {
  @Id
  @Column(nullable = false)
  private Long number;
  @Column(nullable = false)
  private Integer pinCode;
  @ManyToOne
  @JoinColumn(name = "user_id",referencedColumnName = "id",unique = false)
  private User user;

  public Card() {
  }

  public Card(Long number, Integer pinCode, User user) {
    this.number = number;
    this.pinCode = pinCode;
    this.user = user;
  }

  public Long getNumber() {
    return number;
  }

  public void setNumber(Long number) {
    this.number = number;
  }

  public Integer getPinCode() {
    return pinCode;
  }

  public void setPinCode(Integer pinCode) {
    this.pinCode = pinCode;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Card)) {
      return false;
    }
    Card card = (Card) o;
    return Objects.equals(getNumber(), card.getNumber()) &&
        Objects.equals(getPinCode(), card.getPinCode());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getNumber(), getPinCode());
  }

  @Override
  public String toString() {
    return "Card{" +
        "number=" + number +
        ", pinCode=" + pinCode +
        '}';
  }
}
