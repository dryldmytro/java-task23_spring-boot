package com.dryl.domain;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;

@Entity
public class Password {
  @Id
  private Long id;
  @Column(nullable = false)
  private String password;
  @OneToOne(fetch = FetchType.LAZY)
  @MapsId
  private User user;

  public Password() {
  }

  public Password(String password, User user) {
    this.password = password;
    this.user = user;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Password)) {
      return false;
    }
    Password password1 = (Password) o;
    return Objects.equals(getId(), password1.getId()) &&
        Objects.equals(getPassword(), password1.getPassword());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId(), getPassword());
  }

  @Override
  public String toString() {
    return "Password{" +
        "id=" + id +
        ", password='" + password + '\'' +
        '}';
  }
}
