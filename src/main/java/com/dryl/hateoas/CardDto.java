package com.dryl.hateoas;

import com.dryl.domain.Card;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;
public class CardDto extends ResourceSupport {
  Card card;

  public CardDto(Card card, Link link) {
    this.card = card;
    add(link);
  }
  public Long getCardNumber(){
    return card.getNumber();
  }
  public Integer getPinCode(){
    return card.getPinCode();
  }
}
