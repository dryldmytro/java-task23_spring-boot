package com.dryl.hateoas;

import com.dryl.domain.Password;
import com.dryl.domain.User;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
public class PasswordDto extends ResourceSupport {
  Password password;

  public PasswordDto(Password password, Link selfLink) {
    this.password = password;
    add(selfLink);
  }
  public Long getPasswordId(){
    return password.getId();
  }
  public String getPassword(){
    return password.getPassword();
  }
}
