package com.dryl.hateoas;

import com.dryl.controller.CardController;
import com.dryl.controller.PasswordController;
import com.dryl.domain.Password;
import com.dryl.domain.User;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;
 import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;
public class UserDto extends ResourceSupport {
  User user;

  public UserDto(User user, Link selfLink) {
    this.user = user;
    add(selfLink);
    add(linkTo(methodOn(PasswordController.class).getPassword(user.getId())).withRel("password"));
    add(linkTo(methodOn(CardController.class).getCardsByUserID(user.getId())).withRel("cards"));
  }

  public Long getUserId(){
    return user.getId();
  }
  public String getUserName(){
    return user.getName();
  }
}
