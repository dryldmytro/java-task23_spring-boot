package com.dryl.service;

import com.dryl.domain.Card;
import com.dryl.domain.User;
import com.dryl.repository.CardRepository;
import com.dryl.repository.UserRepository;
import java.util.List;
import java.util.Set;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
public class CardService {

  CardRepository cardRepository;
  UserRepository userRepository;

  public CardService(CardRepository cardRepository, UserRepository userRepository) {
    this.cardRepository = cardRepository;
    this.userRepository = userRepository;
  }

  public List<Card> getAllCards() {
    return cardRepository.findAll();
  }

  public Card getCard(Long number) {
    Card card = cardRepository.findById(number).orElseThrow(RuntimeException::new);
    return card;
  }

  public Set<Card> getCardByUserID(Long userId) {
    User user = userRepository.findById(userId).orElseThrow(RuntimeException::new);
    return user.getCards();
  }

  @Transactional
  public void createCard(Card card) {
    cardRepository.save(card);
  }

  @Transactional
  public void deleteCard(Long number) {
    Card card = cardRepository.findById(number).orElseThrow(RuntimeException::new);
    cardRepository.delete(card);
  }
}
