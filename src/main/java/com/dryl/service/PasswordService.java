package com.dryl.service;

import com.dryl.domain.Password;
import com.dryl.repository.PasswordRepository;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
public class PasswordService {
  PasswordRepository passwordRepository;

  public PasswordService(PasswordRepository passwordRepository) {
    this.passwordRepository = passwordRepository;
  }
  @Transactional
  public void createPassword(Password password){
    passwordRepository.save(password);
  }

  public List<Password> getAllPasswords(){
    return passwordRepository.findAll();
  }
  public Password getPassword(Long passwordId){
    Password password = passwordRepository.findById(passwordId).orElseThrow(RuntimeException::new);
    return password;
  }

  @Transactional
  public void updatePassword(Password uPassword, Long id){
    Password password = passwordRepository.findById(id).orElseThrow(RuntimeException::new);
    password.setPassword(uPassword.getPassword());
  }
  @Transactional
  public void deletePassword(Long id){
    Password password = passwordRepository.findById(id).orElseThrow(RuntimeException::new);
    passwordRepository.delete(password);
  }

}
