package com.dryl.service;

import com.dryl.domain.User;
import com.dryl.repository.UserRepository;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
public class UserService {
  UserRepository userRepository;
  CardService cardService;

  public UserService(UserRepository userRepository, CardService cardService) {
    this.userRepository = userRepository;
    this.cardService = cardService;
  }

  public List<User> getAllUsers(){
    return userRepository.findAll();
  }
  public User getUser(Long userId){
    User user = userRepository.findById(userId)
        .orElseThrow(()->new RuntimeException("User doesnt exist."));
    return user;
  }
  @Transactional
  public void createUser(User user){
    userRepository.save(user);
  }
  @Transactional
  public void updateUser(User uUser, Long userId){
    User user = userRepository.findById(userId).orElseThrow(RuntimeException::new);
    user.setName(uUser.getName());
    userRepository.save(user);
  }
  @Transactional
  public void deleteUser(Long userId){
    User user = userRepository.findById(userId).orElseThrow(RuntimeException::new);
    userRepository.delete(user);
  }
}
